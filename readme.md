# cotton

A 2d game framework for assignments in artificial intelligence course at Uppsala University. The whole framework of cotton is in one header-file and one source-file.    

#### namespace rtti
The namespace is used to generate minimal Run-time Type Information for entity classes (RTTI), so we don't have to use the built in system. It is used by the abstract factory in ```EntitySystem```.  
  
rtti can be used instead of defining enums for derived classes to ```Entity```. This is accomplished by using the two macros: ```CLASS_RTTI_DECL``` and ```CLASS_RTTI_DEF```. The first one goes in the class declaration and the second one goes into the definition.  
  
#### namespace utility
```to_string()```, is used to format a string, and ```to_color()``` is used to construct a packed color (unsigned int) in the form of 0x00-0xff.  
  
#### class Math
Contains rudementary mathematical functions including pseudo-random numbers and a non-cryptographical hash function.  
  
#### class Vector2
2D vector class.  
  
```normalize()``` - normalizes the argument vector.  
```length()``` - calculates the length of a vector using square-root.  
```squared_length()``` - calculates the square length of a vector.  
```dot()``` - calculates the angle between two vectors, or area of a resulting  vector.  
```distance()``` - calculates the distance between two vectors.  
```to_angle()``` - calculates the angle of a unit vector.  
```normalized()``` - returns a normalized version of the argument vector.  
```interpolate_linear()``` - linear interpolation between two vectors over normalized t.  
```interpolate_linear()``` - spherical interpolation between two vectors over normalized t.  
  
#### class Rect
Rectangle class.  
  
#### class Time
Elapsed time.  

#### class Clock
Continuum of time.  
  
#### enum KeyCode
Different keycodes.  
  
#### enum MouseButton
Mouse buttons.  
  
#### enum WindowEventType
Operating system event types that the Window class receives and queues for us to poll.  
  
#### class WindowEvent
Class that we can poll from the Window class for keyboard input or mouse input.  
  
#### class Config
A class that parses the command-line arguments sent to the client at startup.  
  
#### class Vertex
Primitive used for drawing. Has position, texture coordinates and color.  
  
#### class Transform
Contains position, scale and rotation for a entity, and origin of the transform (for rotation etc.).  
  
#### class Texture
Texture. Can only load ```.png``` files.  
  
#### class Window
Window class.  
  
#### class Font
Used for drawing text inside a window. Only works with true-type fonts (```.ttf```).  
  
#### class Audio
Class for managing an audio context. We need one before we can play sound effects or music.  
  
#### class Sound
Used for sound effects i.e audio with short duration. Audio files must be 44100hz and ```.ogg```.  

#### class Music
Background music, etc. See above regarding format.  
  
#### class Sprite
A class for visual representation.  
  
#### class Entity
Base class for all game objects.  
  
#### class EntitySystem
Think of this as our old trusted ```EntityManager```. This has a ```AbstractFactory``` for Entity. We can query entities within a circle.    
  