// main.cpp

#include "cotton.hpp"

#define GUIID (__LINE__)

namespace cotton
{
  class Keyboard
  {
  public:
    Keyboard()
    {
      memset(m_current, 0, sizeof(m_current));
      memset(m_previous, 0, sizeof(m_previous));
    }

    bool is_down(KeyCode keycode) const { return m_current[keycode]; }
    bool is_down_once(KeyCode keycode) const { return m_current[keycode] && !m_previous[keycode]; }
    void update() { memcpy(m_previous, m_current, sizeof(m_previous)); }
    void on_event(const WindowEvent& event)
    {
      switch (event.get_type())
      {
        case kWindowEventType_KeyDown:
        {
          if (!m_current[event.get_keycode()])
          {
            m_current[event.get_keycode()] = true;
          }
        } break;
        case  kWindowEventType_KeyUp:
        {
          m_current[event.get_keycode()] = false;
        } break;
      }
    }

  private:
    bool m_current[kKeyCode_Invalid];
    bool m_previous[kKeyCode_Invalid];
  };

  class Mouse
  {
  public:
    Mouse()
    {
      memset(m_current, 0, sizeof(m_current));
      memset(m_previous, 0, sizeof(m_previous));
    }

    bool is_down(MouseButton button) const { return m_current[button]; }
    bool is_down_once(MouseButton button) const { return m_current[button] && !m_previous[button]; }
    void update() { memcpy(m_previous, m_current, sizeof(m_previous)); m_cursor_previous = m_cursor_current; }
    Vector2 get_cursor() const { return m_cursor_current; }
    Vector2 get_cursor_delta() const { return m_cursor_current - m_cursor_previous; }
    void on_event(const WindowEvent& event)
    {
      switch (event.get_type())
      {
        case kWindowEventType_MouseMove:
        {
          m_cursor_current = Vector2(event.get_x(), event.get_y());
        } break;
        case kWindowEventType_MouseDown:
        {
          m_current[event.get_button()] = true;
        } break;
        case kWindowEventType_MouseUp:
        {
          m_current[event.get_button()] = false;
        } break;
      }
    }

  private:
    bool m_current[kMouseButton_Invalid];
    bool m_previous[kMouseButton_Invalid];
    Vector2 m_cursor_current;
    Vector2 m_cursor_previous;
  };

  class Terrain
  {
  public:
    Terrain(const Texture* texture, unsigned width, unsigned height)
      : m_batch(texture)
    {
      const int num_widths = 1;
      const int num_heights = 2;

      int pw = 64, ph = 64;
      int num_tiles_x = (width / pw + 1) * num_widths;
      int num_tiles_y = (height / ph + 1) * num_heights;

      Rect texs[] =
      {
        Rect(1, 1, 30, 30),
        Rect(33, 1, 30, 30),
        Rect(65, 1, 30, 30),
      };

      for (int y = 0; y < num_tiles_y; y++)
      {
        int py = y * ph;
        for (int x = 0; x < num_tiles_x; x++)
        {
          int px = x * pw;
          unsigned type = ((unsigned)Math::random(0.0f, (float)ArraySize(texs))) % ArraySize(texs);
          Rect pos(px, py, pw, ph);
          m_batch.push(pos, texs[type]);
        }
      }
    }

    void draw(const Camera& camera, const Window& window)
    {
      window.draw(camera.transform(m_transform), m_batch.get_vertices(), m_batch.get_vertex_count(), m_batch.get_texture());
    }

  private:
    Transform m_transform;
    SpriteBatch m_batch;
  };  
}

using namespace cotton;

int main(int argc, char** argv)
{
  Config config(argc, argv);
  
  Window window;
  if (!window.open(config.m_width, config.m_height, "cotton")) return -1;

  Audio audio;
  if (!audio.open(window)) return -2;

  Clock clock;
  float accumulated = 0.0f;

  Texture gui_texture;
  gui_texture.create_from_filename("assets/gui.png");
  gui::initialize(window, &gui_texture);

  Mouse mouse;
  Keyboard keyboard;

  const int num_widths = 1;
  const int num_heights = 2;
  Camera camera(window);
  {
    int bounds_width = window.get_width() * num_widths - window.get_width();
    int bounds_height = window.get_height() * num_heights - window.get_height();
    Rect bounds(0, 0, bounds_width, bounds_height);
    camera.set_bounds(bounds);
  }

  Font font;
  font.create_from_filename("assets/FiraMono.ttf", 18.0f);

  Texture texture;
  texture.create_from_filename("assets/minecraft.png");

  Terrain terrain(&texture, window.get_width(), window.get_height());
  
  bool running = true;
  while (running && window.is_open())
  {
    Time deltatime = clock.get_elapsed_time();
    clock.reset();

    accumulated += deltatime.as_seconds();

    std::string title = utility::to_string(
      "cotton - fps: %d time: %ds",
      (int)(1.0f / deltatime.as_seconds()), 
      (int)accumulated);
    window.set_title(title.c_str());

    WindowEvent event;
    while (window.poll_event(event))
    {  
      mouse.on_event(event);
      keyboard.on_event(event);
      gui::on_event(event);

      switch (event.get_type())
      {
        case kWindowEventType_Quit:
        {
          running = false;
        } break;
      }
    }
    if (mouse.is_down(kMouseButton_Left))
    {
      camera.move(-mouse.get_cursor_delta());
    }

    gui::prepare();
    if (gui::button(GUIID, 10, 10, 200, 40, "Button"))
    {
      printf("clickety!\n");
    }

    window.clear();
    terrain.draw(camera, window);
    gui::flush();
    window.present();

    audio.present();
    mouse.update();
    keyboard.update();
  }

  audio.close();
  window.close();
  
  return 0;
}
